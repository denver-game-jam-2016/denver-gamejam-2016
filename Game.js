var BrainGame = { };

BrainGame.Game = function(game) {
    this.platformGroup;
    this.player;
    this.badMemoriesGroup;
    this.goodThoughtsGroup;
    this.progressBar;
    this.playableBounds;
};

BrainGame.Game.prototype = {
    preload:function() {
        
        this.load.image('background', 'images/brain-background.jpg');
        this.load.image('bad-memory', 'images/placeholder/bad-memory.png');
        
        //floor tiles
        this.game.load.image("floor0", "images/placeholder/floor-tiles/floor1.png");
        this.game.load.physics("floor0_physics", "images/placeholder/floor-tiles/floor1.json");
        
        //progress bar
        this.load.image('progressBarBackground', 'images/status-bar-background.png');
        this.load.image('progressBarForeground', 'images/status-bar-foreground.png');
        
        //load the gnome and then never use those two words together again
        this.load.image('gnome', 'images/placeholder/gnome.png');
        
        // player sprite
        this.load.spritesheet('dude', 'images/player_sprite_v1.png', 27, 59);
    },
    
    create:function() {
//        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.physics.startSystem(Phaser.Physics.P2JS);

        this.buildWorld();
        this.playableBounds = this.getPlayableBounds();
        this.buildBadMemories();
        
        // Get the player
        this.buildPlayer();

        //controls
        this.cursors = this.input.keyboard.createCursorKeys();
        
        //HUD elements last so they are always on top
        this.buildProgressBar();
    },
    
    update:function() {
        this.updateProgressBar();
        
//        this.physics.arcade.collide(this.player, this.badMemoriesGroup);

        // Default to player not moving
        this.player.body.velocity.x = 0;
        var cursors = this.cursors;
        
        // Player collide with platforms
//        this.physics.arcade.collide(this.player, this.platformGroup);
        
        // Move character with arrow keys
        if (cursors.left.isDown)
            {   //Move to the left
                this.player.body.velocity.x = -50;
                this.player.animations.play('moonWalk');
            }

        else if (cursors.right.isDown)
           {   //Move to the right
                this.player.body.velocity.x = 50;
                this.player.animations.play('right');
           } 

        else
            {  //Stand still
                this.player.animations.stop();
                this.player.frame = 4;
            }
        
        // Allow jump if touching the ground
        if (cursors.up.isDown)
            {
                this.player.body.velocity.y = -350;
            }
    },
    
    buildBadMemories:function() {
        this.badMemoriesGroup = this.add.group();
        
        //spawn some randome ones through the world in the playable bounds
        var numBadMemories = 50;
        for (var i = 0; i<numBadMemories; i++)
        {
            var location = this.getRandomCoordinatesInPlayableBounds();
            var badGuy = this.badMemoriesGroup.create(location.x, location.y, 'bad-memory');
            badGuy.enableBody = true;
            this.physics.p2.enable(badGuy);
//            badGuy.body.immovable = true; //arcade physics
        }
        
        this.badMemoriesGroup.forEach(function(m) {
            m.scale.x = .1;
            m.scale.y = .1;
        });
        
        this.buildBadMemories.enableBody = true;
    },
    //give ourselves some buffer space so we don't spawn in floor/ceiling/beginning/end areas
    getPlayableBounds: function() {
        var bounds = {
            buffer: {
                x: 400,
                y: 150,
            },
        };
        
        bounds.start = {
            x: bounds.buffer.x,
            y: bounds.buffer.y,
        };
        
        bounds.end = {
            x: this.world.width - bounds.buffer.x,
            y: this.world.height - bounds.buffer.y,
        };
        
        return bounds;
    },
    
    getRandomCoordinatesInPlayableBounds:function() {
        var bounds = this.playableBounds;
        var x = this.rnd.integerInRange(bounds.start.x, bounds.end.x);
        var y = this.rnd.integerInRange(bounds.start.y, bounds.end.y);
        return {
            x: x,
            y: y,
        };
    },
    
    updateProgressBar:function() {
        
        this.progressBar.foregroundSprite.width = this.progressBar.maxWidth * this.progressBar.fillPercentage;
    },
    
    buildPlayer:function(){
        // Set up the player
        this.player = this.add.sprite(0, 475, 'dude');
        this.physics.p2.enable(this.player);
        
        this.player.restitution = .2; //aka bounce
//        this.player.body.bounce.y = 0.2;
//        this.player.body.gravity.y = 300;
//        this.player.body.collideWorldBounds = true;

        // Create animations for player
        this.player.animations.add('moonWalk', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [4, 5, 6, 7, 6, 5, 4], 5, true);     
        
        // Camera follow player
        this.camera.follow(this.player);  
    },
    
    buildProgressBar:function() {
        //defaults
        this.progressBar = {
            fillPercentage: .1,
            x: 100,
            y: 100,
            maxWidth: 200,
            height: 30,
        };
        this.progressBar.backgroundSprite = this.add.tileSprite(this.progressBar.x, this.progressBar.y, this.progressBar.maxWidth, this.progressBar.height, 'progressBarBackground');
        this.progressBar.backgroundSprite.fixedToCamera = true;
        
        this.progressBar.foregroundSprite = this.add.tileSprite(this.progressBar.x, this.progressBar.y, this.progressBar.maxWidth, this.progressBar.height, 'progressBarForeground');
        this.progressBar.foregroundSprite.fixedToCamera = true;
        
        this.updateProgressBar(); //call once here to set initial fill
    },
    
    buildWorld:function() {
        this.world.setBounds(0, 0, this.world.width * 9, this.world.height * 1);
        this.add.tileSprite(0, 0, this.world.width, this.world.height, 'background');

        //todo build platforms the player interacts with
        //var ground = this.platformGroup.createMultiple(-10, 520, 'gnome');
        //var ceiling = this.platformGroup.createMultiple(-10, 55, 'gnome');
        
        this.add.tileSprite(0, 500, this.world.width, 40, 'gnome');
        
        this.platformGroup = this.add.group();
        this.platformGroup.enableBody = true;
//        
//        for (var i = 1; i < 50; i++)
//            {
//                this.platformGroup.create(1 + i++, 500, 'gnome');
//            }
//        
//        this.platformGroup.forEach(function (child) {
//           child.scale.x = .08;
//           child.scale.y = .08;
//        });

        var ground = this.platformGroup.create(-10, 520, 'gnome');
        var ceiling = this.platformGroup.create(-10, 55, 'gnome');
        
        // shrink platform sprites
        ground.scale.x = .08;
        ground.scale.y = .08;
        ceiling.scale.x = .08;
        ceiling.scale.y = .08;
        
        ground.body.immovable = true;
        ceiling.body.immovable = true;
    },

};